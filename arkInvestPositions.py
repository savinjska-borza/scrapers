import csv

from fake_useragent import UserAgent
import os
import requests
import re
import pymysql
import time
import datetime

try:
    u_agent = UserAgent()
except Exception as e:
    print("PATH:" + str(os.getcwd() + 'fakeUserAgents.json'))
    u_agent = UserAgent(path=os.getcwd() + 'fakeUserAgents.json')
    pass

c = None
conn = None

def dbConnect():
    global c
    global conn
    try:
        c = None
    except:
        pass
    try:
        conn = None
    except:
        pass

    conn = pymysql.connect(
        db="stockbot",
        user='root',
        passwd='',
        host='localhost',
        use_unicode=True, charset="utf8")
    c = conn.cursor(pymysql.cursors.DictCursor)
    c.execute('SET NAMES utf8;')
    c.execute('SET CHARACTER SET utf8;')
    c.execute('SET character_set_connection=utf8;')


from time import sleep
#_------------------------------


class arkPosition:
    def __init__(self):
        self.arrayOfHoldings = [
            "https://ark-funds.com/wp-content/fundsiteliterature/csv/ARK_INNOVATION_ETF_ARKK_HOLDINGS.csv",
            "https://ark-funds.com/wp-content/fundsiteliterature/csv/ARK_AUTONOMOUS_TECHNOLOGY_&_ROBOTICS_ETF_ARKQ_HOLDINGS.csv",
            "https://ark-funds.com/wp-content/fundsiteliterature/csv/ARK_NEXT_GENERATION_INTERNET_ETF_ARKW_HOLDINGS.csv",
            "https://ark-funds.com/wp-content/fundsiteliterature/csv/ARK_GENOMIC_REVOLUTION_MULTISECTOR_ETF_ARKG_HOLDINGS.csv",
            "https://ark-funds.com/wp-content/fundsiteliterature/csv/ARK_FINTECH_INNOVATION_ETF_ARKF_HOLDINGS.csv",
                           ]

    def csv2SQL(self, csvFile, tableName):
        # Open CSV and start output
        with open(csvFile, encoding="utf8") as f:
            reader = csv.reader(f, delimiter=',')
            # Create the header row, since we may have to repeat it
            header_row = 'INSERT INTO ' + tableName + ' ('
            first = True
            for item in next(reader):
                if first:
                    first = False
                else:
                    header_row += ', '
                header_row += item.replace('"', "").replace('($)', "").replace(' ', "_").replace("weight(%)",
                                                                                                 "weight").replace(
                    '_/_', '_').replace('Š', 'S').replace('š', 's').replace('Č', 'Č').replace('č', 'c').replace('Ž',
                                                                                                                'Z').replace(
                    'ž', 'z')
            header_row += ') VALUES '

            # Set a counter, since there can't be more than 1000 inserts at a time
            counter = 0
            # Loop through the rows...
            print(header_row)
            for row in reader:
                queryData = ""
                queryData += "("
                first = True

                # Loop through the items in each row
                for item in row:
                    if first:
                        first = False
                    else:
                        queryData += ', '
                    queryData += '\'' + item.replace('\'', '\'\'').replace('"', '\'').replace('&', '&.').replace('""',
                                                                                                                 '"').replace(
                        '"', '') + '\''
                queryData += ')'
                # Increase counter
                counter += 1
                queryData += ';'
                try:
                    query = header_row + queryData
                    c.execute(query)
                    print("INSERTED:" + str(query))
                    if counter % 1000 == 0:
                        conn.commit()
                except Exception as e:
                    if ("Duplicate entry" in str(e)):
                        pass
                    else:
                        print(e)
                        print(header_row + queryData)
            conn.commit()


    def getAllHoldingsAndImportThemToDB(self):
        for holding in self.arrayOfHoldings:
            headers={"user-agent": str(u_agent.random)}
            response = requests.get(holding, headers=headers,verify=False)
            open(holding.split("/")[-1], 'wb').write(response.content)
            self.csv2SQL(holding.split("/")[-1],"ark")
            sleep(5)
            print("DATA SUCCESSFULLY IMPORTED FOR {}".format(holding.split("/")[-1]))

def updateArkInvestDate(date, id):
    date = datetime.datetime.fromtimestamp(date).strftime('%Y-%m-%d %H:%M:%S')
    res = c.execute("UPDATE ark SET timestamp='{0}' WHERE id LIKE '{1}'".format(date, id))
    conn.commit()
    pass

def getArkInvestData():
    c.execute("""SELECT * FROM ark  WHERE timestamp IS NULL""")
    data = c.fetchall()
    return data


def addTimestamp():
    # a = "12/7/2020"
    # b  = time.mktime(datetime.datetime.strptime(a, "%m/%d/%Y").timetuple())
    # c = datetime.datetime.fromtimestamp(b).strftime('%Y-%m-%d %H:%M:%S')
    for item in getArkInvestData():
        if(len(item['date'])>1):
            updateArkInvestDate(time.mktime(datetime.datetime.strptime(item['date'], "%m/%d/%Y").timetuple()), item['id'])

while(1):
    dbConnect()
    arkPositionsObject = arkPosition()
    arkPositionsObject.getAllHoldingsAndImportThemToDB()
    addTimestamp()
    print("WAIT FOR 10 HOURS...")
    time.sleep(36000)