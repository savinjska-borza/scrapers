import sys
import os
from fake_useragent import UserAgent
from urllib.request import Request, urlopen
from bs4 import BeautifulSoup
import requests
import traceback
from time import sleep
from fake_useragent import UserAgent
import json
u_agent = UserAgent()
import pymysql
from datetime import datetime, timedelta
# Connect to DB
conn = pymysql.connect(
    db="stockbot",
    user='root',
    passwd='',
    host='localhost',
    use_unicode=True, charset="utf8")
c = conn.cursor(pymysql.cursors.DictCursor)
c.execute('SET NAMES utf8;')
c.execute('SET CHARACTER SET utf8;')
c.execute('SET character_set_connection=utf8;')

def sendRequestToAPI(method, route, data={}):
    if (method == "POST"):
        url = route
        try:
            r = requests.post(url=url, data=data)
            return r.json()

        except Exception as e:
            print("napaka pri zahtevku :" + str(traceback.format_exc()))

    if (method == "GET"):
        url = APIURL+"/api/v1/" + route
        try:
            r = requests.get(url=url)
            return r.json()

        except Exception as e:
            print("napaka pri zahtevku :" + str(traceback.format_exc()))

def findBetween(s, first, last):
    try:
        start = s.index(first) + len(first)
        end = s.index(last, start)
        return s[start:end]
    except ValueError:
        return ""

def getListOfAllStocks(type):
    if(type == "totalScore"):
        #Order by total Score
        payload= {"id":"0","no_result_if_limit":"false","offset":0,"size":24,"state":"read","rules":"[[\"value_score\",\">=\",0],[\"dividends_score\",\">=\",0],[\"future_performance_score\",\">=\",0],[\"health_score\",\">=\",0],[\"past_performance_score\",\">=\",0],[\"grid_visible_flag\",\"=\",true],[\"market_cap\",\"is_not_null\"],[\"primary_flag\",\"=\",true],[\"is_fund\",\"=\",false],[\"order_by\",\"total_score\",\"desc\"],[\"aor\",[[\"exchange_symbol\",\"in\",[\"nyse\"]]]]]"}


    if(type == "marketcap"):
        #Order by marketcap
        payload = {"id":"0","no_result_if_limit":"false","offset":0,"size":30,"state":"create",
                                    "rules":"[[\"value_score\",\">=\",0],[\"dividends_score\",\">=\",0],"
                                            "[\"future_performance_score\",\">=\",0],[\"health_score\",\">=\",0],"
                                            "[\"past_performance_score\",\">=\",0],[\"grid_visible_flag\",\"=\",true],"
                                            "[\"market_cap\",\"is_not_null\"],[\"primary_flag\",\"=\",true],"
                                            "[\"is_fund\",\"=\",false],[\"order_by\",\"market_cap\",\"desc\"]]"}


    return sendRequestToAPI("POST", 'https://api.simplywall.st/api/grid/filter?include=info,score', payload)

def getDataForStock(url):
    print(url)
    req = Request(url)
    req.add_header('User-Agent', u_agent.random)  # Random user agent iz W3 schools
    req.add_header('Accept-Language', 'en')
    respons_data = urlopen(req).read().decode('utf8')
    soup = BeautifulSoup(respons_data, "html.parser")
    return soup


def insertNewStockScreen(response, url, type):
    date = datetime.now().strftime('%d-%m-%Y %H:%M:%S')
    loggit = """
                    INSERT INTO  screeners (response,  date, link, status, type)
                    VALUES
                        (%s, %s, %s,  %s, %s)
                """
    c.execute(loggit, (response, date, url, 0, type))
    conn.commit()
    print("INSERTED-INDEX")


def insertNewIndex(stock):
    loggit = """
                    INSERT INTO  index_data (companyID,  companyUID, trading_item_id, company_name, slug, exchange_symbol, ticker, uticker, last_updated,  canonical_url, isin, info, score)
                    VALUES
                        (%s, %s, %s,  %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
                """
    c.execute(loggit, (stock['id'], stock['company_id'], stock['trading_item_id'], stock['name'], stock['slug'], stock['exchange_symbol'], stock['ticker_symbol'], stock['unique_symbol'], stock['last_updated'], stock['canonical_url'], stock['isin_symbol'], str(stock['info']['data']), str(stock['score']['data'])))
    conn.commit()
    print("INSERTED")


def getAndInsertNewStockScreens(type):
    listOfAllStocks = getListOfAllStocks(type)
    print("Number of stocks:")
    print(len(listOfAllStocks['data']))
    #Insert indexes
    for stock in listOfAllStocks['data']:
        insertNewIndex(stock)
    #Get detailed info for indexes
    for stock in listOfAllStocks['data']:
        print(stock['canonical_url'])
        soup = getDataForStock("https://simplywall.st"+stock['canonical_url'])
        data = findBetween(str(soup), "window.REDUX_STATE = ", "</script>").replace('new Map([])',"").replace('"entities": {},','"entitiesx": [],').replace('"entities":','"entities": []').replace('""{}','[]').replace(': []{},',': [],').replace('undefined','[]')
        print(data)
        parsedJSON = json.loads(data)
        #print(parsedJSON['company']['statements'])
        #print(parsedJSON['company']['news'])
        #print(parsedJSON['company']['analysis'])
        insertNewStockScreen(data, stock['canonical_url'], type)
        #zapisem v bazo celi redux JSON, zravn dodam datum..potem, gre pa on druge cez in bo v tabelo za predikcije naredil predikcije
        sleep(100)

while(1):
    print("---------START-------------")
    getAndInsertNewStockScreens("totalScore")
    getAndInsertNewStockScreens("marketcap")
    #Vsakih 12 ur dobim podatke
    sleep(43200)





