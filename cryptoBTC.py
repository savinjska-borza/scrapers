import math
import ccxt
from time import sleep
import winsound
import pymysql
from coinmarketcapapi import CoinMarketCapAPI, CoinMarketCapAPIError
import cachetools.func
cmc = CoinMarketCapAPI('43906e39-4acd-463a-98e8-3e302c0d6033')
binance     = ccxt.binance({
    'apiKey': 'EG3rpPz9lcJEpDr5gAYG4dvyJTSF2JEglcnZ61vA9JT2cIyT1NlBXPnhAh65SIj9',
    'secret': 'FIoalbsfX7vu6Woi1UcBPQWGU57AUlYRWe6Pa4uAX63qnn08OtxFGraDxKHrwhIy',
    'timeout': 30000,
    'enableRateLimit': True,
})

c = None
conn = None

def buySound():
    frequency = 2300  # Set Frequency To 2500 Hertz
    duration = 500  # Set Duration To 1000 ms == 1 second
    winsound.Beep(frequency, duration)

def sellSound():
    frequency = 1300  # Set Frequency To 2500 Hertz
    duration = 100  # Set Duration To 1000 ms == 1 second
    winsound.Beep(frequency, duration)
    frequency = 2300  # Set Frequency To 2500 Hertz
    duration = 100  # Set Duration To 1000 ms == 1 second
    winsound.Beep(frequency, duration)
    frequency = 1300  # Set Frequency To 2500 Hertz
    duration = 100  # Set Duration To 1000 ms == 1 second
    winsound.Beep(frequency, duration)

@cachetools.func.ttl_cache(maxsize=128, ttl=10 * 60)
def getCurrentPriceFromCoinMarketCap():
    r = cmc.cryptocurrency_listings_latest()
    a = r.data
    for item in a:
        if (item['symbol'] == "BNB"):
            price = item['quote']['USD']['price']
            return price


def dbConnect():
    global c
    global conn
    try:
        c = None
    except:
        pass
    try:
        conn = None
    except:
        pass

    conn = pymysql.connect(
        db="crypto",
        user='root',
        passwd='',
        host='localhost',
        use_unicode=True, charset="utf8")
    c = conn.cursor(pymysql.cursors.DictCursor)
    c.execute('SET NAMES utf8;')
    c.execute('SET CHARACTER SET utf8;')
    c.execute('SET character_set_connection=utf8;')

def addNewOrder(ticker, order_data, market_price, minimal_sell, orderType, status, fixedOrderID, profit , strikePrice = None):
    dbConnect()
    query = """
              INSERT INTO  orders2 (ticker, order_data, market_price, minimal_sell, order_type, status, fixed_order_id, profit, strike_price, bnb_price)
              VALUES
              (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
     """
    c.execute(query, (ticker, str(order_data), market_price, minimal_sell, orderType, status, fixedOrderID, profit, strikePrice, getCurrentPriceFromCoinMarketCap()))
    conn.commit()
    return 200

def getMarketPrice(symbol):
    orderbook = binance.fetch_order_book (binance.markets[symbol]['symbol'] )
    bid = orderbook['bids'][0][0] if len (orderbook['bids']) > 0 else None
    ask = orderbook['asks'][0][0] if len (orderbook['asks']) > 0 else None
    spread = (ask - bid) if (bid and ask) else None
    #print (binance.id, 'market price', { 'bid': bid, 'ask': ask, 'spread': spread })
    return bid


def getMinimalSell(price, symbol, ticker):
    #0.03145, TRX/USDT , TRX
    a = binance.markets[symbol]
    minCost = round(a['limits']['cost']['min'] / price, 6)
    minCost = format(float(minCost) + float(150 * minCost) / 100, 'f') #add 2 %
    return minCost

def getAllBuyOrders():
    try:
        c.commit()
    except:
        pass
    c.execute("""SELECT * FROM orders2 WHERE status LIKE 0 AND order_type = 'BUY'""")
    data = c.fetchall()
    return data

def updateBuyOrderStatus(buyOrderID):
    res = c.execute(
        "UPDATE orders2 SET status=1 WHERE id LIKE '{0}'".format(buyOrderID))
    conn.commit()

buyPriceFloor =  19000 #34000
buyPrice = 0 #array of prices
decreesBuyPriceAfterBuy =  50 #For how much buy price will be decresed after buy (so it will not buy too fast again)
maxNumberOfBuyOrders = 5
requiredNumberOfTicksToWaitBeforeNextBuyOrder = 0
requiredNumberOfTicksToWaitBeforeNextBuyOrderSet = 40 #How much tick to wait before to do another BUY
stakeSellPrice = 190  #For how much sell price must be bigger then buy price
numberToDecresseBuyPriceAfterSuccessfullSell = 20 #For how much buy price will be decressed after successull sell
numberToDecresseBuyPriceAfterBotStarted = 40 #Decrese price for 50 $
numberOfDiffBetweenBuyPriceAndMarketPriceToResetBuyPrice = 100 # diff before reset
numberToInreaseStakeSellPriceAfterSell = 15
lastSellTicker = 0
lastSellTickerReset = 15
configTicker = "BTC/USDT"


def clearanceHouse(marketPrice, minimalSell, buyMarketOrders):
    """:arg
    #It will 'fix' orders in DB (FIX = sell at profitable price)
    """
    global buyPrice
    global stakeSellPrice
    for order in buyMarketOrders:
        orderSellPrice = float(order['market_price']) + stakeSellPrice
        if(marketPrice>=orderSellPrice):
            #Izvedem nakup če je bid price (buy) večja ali enaka definirani ter če ni zadnji nakup bil SELL
            binanceOrder = binance.id, binance.create_market_sell_order(configTicker, minimalSell)
            print(f"****SELL ORDER EXECUTED: MARKET PRICE: {marketPrice}, NUMBER OF BTC: {minimalSell}, FIXED ORDER ID {order['id']}")
            profit = format(float(order['minimal_sell']) - float(minimalSell), "f")
            buyPrice = buyPrice - float(numberToDecresseBuyPriceAfterSuccessfullSell) #Set buy price #todo buy price range... definiran rang nad kolko ne kupuje
            stakeSellPrice  = float(stakeSellPrice) + float(numberToInreaseStakeSellPriceAfterSell)
            addNewOrder(configTicker, str(binanceOrder), str(marketPrice), str(minimalSell), "SELL", 1, order['id'], profit)
            updateBuyOrderStatus(order['id'])
            sellSound()
            continue

print(binance.fetch_balance())

dbConnect()
#Set Buy price
marketPrice = getMarketPrice(configTicker)
buyPrice = float(marketPrice) - float(numberToDecresseBuyPriceAfterBotStarted)
while(1):
    try:
        sleep(1)
        buyMarketOrders = getAllBuyOrders()
        marketPrice = getMarketPrice(configTicker)
        print(f"Market Price: {marketPrice}, Stake sell price: {stakeSellPrice},Buy price floor: {buyPriceFloor}, BuyPrice: {buyPrice}, Diff price (Market/Buy): {float(marketPrice)-float(buyPrice)}, TickersLeft: {requiredNumberOfTicksToWaitBeforeNextBuyOrder}, Number of Buy orders {len(buyMarketOrders)}/{maxNumberOfBuyOrders}")
        minimalSell = getMinimalSell(marketPrice, configTicker , "BTC")
        print(minimalSell)
        #If diff between buy price and market price is to high we reset price
        if(abs(float(marketPrice)-float(buyPrice))>numberOfDiffBetweenBuyPriceAndMarketPriceToResetBuyPrice):
            buyPrice = float(marketPrice) - float(numberToDecresseBuyPriceAfterBotStarted) #Reset buyPrice since difference was to high
            requiredNumberOfTicksToWaitBeforeNextBuyOrder = 20
            print("---BUY PRICE RESETED---")

        if(marketPrice<=buyPrice  and len(buyMarketOrders)<maxNumberOfBuyOrders and requiredNumberOfTicksToWaitBeforeNextBuyOrder<1 and buyPrice<=buyPriceFloor):
            if(abs(requiredNumberOfTicksToWaitBeforeNextBuyOrder)<100):
                if (abs(requiredNumberOfTicksToWaitBeforeNextBuyOrder) <= 10):
                    requiredNumberOfTicksToWaitBeforeNextBuyOrder = requiredNumberOfTicksToWaitBeforeNextBuyOrderSet + 1750
                if (abs(requiredNumberOfTicksToWaitBeforeNextBuyOrder) >= 10 and abs(requiredNumberOfTicksToWaitBeforeNextBuyOrder) <= 30):
                    requiredNumberOfTicksToWaitBeforeNextBuyOrder = requiredNumberOfTicksToWaitBeforeNextBuyOrderSet + 1000
                else:
                    requiredNumberOfTicksToWaitBeforeNextBuyOrder = requiredNumberOfTicksToWaitBeforeNextBuyOrderSet + 50
            else:
                requiredNumberOfTicksToWaitBeforeNextBuyOrder = requiredNumberOfTicksToWaitBeforeNextBuyOrderSet
            #Izvedem nakup če je bid price (buy) majnša ali enaka definirani ter če ni zadnji nakup bil BUY
            order = binance.id, binance.create_market_buy_order(configTicker, minimalSell)
            print(f"****BUY ORDER EXECUTED: MARKET PRICE: {marketPrice}, NUMBER OF BTC: {minimalSell}")
            strikePrice = float(marketPrice) +  float(stakeSellPrice)
            print(f"Strike price: {str(strikePrice)}")
            addNewOrder(configTicker, str(order), str(marketPrice), str(minimalSell), "BUY", 0, None, 0, strikePrice = strikePrice  )
            buyPrice = buyPrice - decreesBuyPriceAfterBuy
            #todo hitreje ko gre gor visji je stakeSellPrice
            stakeSellPrice = stakeSellPrice - numberToInreaseStakeSellPriceAfterSell/5 #Ko kupi malo zmajnsa za kolko more bit razlika
            buySound()
            continue
        requiredNumberOfTicksToWaitBeforeNextBuyOrder+=-1
        #Try to "fix" (execute pairs for buy orders)
        clearanceHouse(marketPrice, minimalSell, buyMarketOrders)

    except Exception as e:
        print(e)
        dbConnect()
        sleep(10)
        pass