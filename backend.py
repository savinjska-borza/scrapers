from flask import Flask, jsonify, request
from waitress import serve
from flask_cors import CORS
from flaskext.mysql import MySQL
import functools
import json
import os
from os.path import join, dirname
from dotenv import load_dotenv
import DB
# ----------ENV-------
dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)
APIKEY = os.environ.get("API_KEY")
# --------------------
# App
app = Flask(__name__)

# cors
cors = CORS(app, resources={r"/*": {"origins": "*"}})
DB.mySqlConnection(app)
# Authentication
def authenticate(f):
    # type: (Callable[..., Any]) -> Callable[..., Any]
    """
    A function wrapper for Authorization.
    """

    @functools.wraps(f)
    def main(*args, **kwargs):
        if (request.headers.get("Authorization") == APIKEY):
            print("REQUEST RECEIVED: ", str(request.url), str(request.headers), "Remote addr: " + str(request.remote_addr))
            print("------------------------------------------------------------")
            return f(*args, **kwargs)
        result = {'Error': 'API Key is not valid.'}
        print(result, str(request.url), str(request.headers))
        return jsonify(result), 403
    return main


@app.route('/ark', methods=['GET'])
@app.route('/news', methods=['GET'])
@app.route('/reddit_posts', methods=['GET'])
@authenticate
def data():
    """:arg
    Data
    """
    DB.mySqlConnection(app)
    if (request.method == "GET"):
        if ("column" in request.args and "value" in request.args):
            # Single product
            colmun = request.args.get("column")
            value = request.args.get("value")
            endpoint = str(request.path).replace("/", "")
            result = DB.get().data(colmun, value, endpoint)
        else:
            # All products
            endpoint = str(request.path).replace("/","")
            result = DB.get().data(tableName=endpoint)
        return jsonify(result)

@app.route('/news-filters', methods=['GET'])
@app.route('/ark-filters', methods=['GET'])
@app.route('/reddit_posts-filters', methods=['GET'])
@authenticate
def columns():
    """:arg
    Column names
    """
    DB.mySqlConnection(app)
    endpoint = str(request.path).split("-")[0].replace("/","")
    result = DB.get().columns(endpoint)
    return jsonify(result)



if __name__ == '__main__':
    # Debug
    #app.run(host='0.0.0.0', debug=True, port=6062)
    # Prod PORT IS OPEN
    serve(app, port=8133, threads=10)


#Cronew za skrejpanje ostalih virov
#Napisat dokumentacijo za vse vire katere imama
#Če še nimam API za vir ga moram dodati
#vračat cene delnic