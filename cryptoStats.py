import math
import ccxt
from time import sleep
import winsound
import pymysql
import json

binance     = ccxt.binance({
    'apiKey': 'EG3rpPz9lcJEpDr5gAYG4dvyJTSF2JEglcnZ61vA9JT2cIyT1NlBXPnhAh65SIj9',
    'secret': 'FIoalbsfX7vu6Woi1UcBPQWGU57AUlYRWe6Pa4uAX63qnn08OtxFGraDxKHrwhIy',
    'timeout': 30000,
    'enableRateLimit': True,
})

def dbConnect():
    global c
    global conn
    try:
        c = None
    except:
        pass
    try:
        conn = None
    except:
        pass

    conn = pymysql.connect(
        db="crypto",
        user='root',
        passwd='',
        host='localhost',
        use_unicode=True, charset="utf8")
    c = conn.cursor(pymysql.cursors.DictCursor)
    c.execute('SET NAMES utf8;')
    c.execute('SET CHARACTER SET utf8;')
    c.execute('SET character_set_connection=utf8;')

dbConnect()

def getOrderByID(id, ticker):
    if(ticker == "BTC/USDT"):
        c.execute(f"""SELECT * FROM orders2 WHERE id LIKE {id} """)
        data = c.fetchone()
        return data
    if(ticker == "TRX/USDT"):
        c.execute(f"""SELECT * FROM orders WHERE id LIKE {id} """)
        data = c.fetchone()
        return data

##
def getAllFinishedOrders(ticker):
    if(ticker == "BTC/USDT"):
        c.execute("""SELECT * FROM orders2 WHERE status LIKE 1 """)
        data = c.fetchall()
        return data
    if(ticker == "TRX/USDT"):
        c.execute("""SELECT * FROM orders WHERE status LIKE 1 """)
        data = c.fetchall()
        return data

def getPrice(symbol):
    orderbook = binance.fetch_order_book (binance.markets[symbol]['symbol'] )
    bid = orderbook['bids'][0][0] if len (orderbook['bids']) > 0 else None
    ask = orderbook['asks'][0][0] if len (orderbook['asks']) > 0 else None
    spread = (ask - bid) if (bid and ask) else None
    #print (binance.id, 'market price', { 'bid': bid, 'ask': ask, 'spread': spread })
    return bid


def getStats(orders, ticker):
    totalProfit = 0.0
    totalProfitUSDTBuy = 0.0
    totalProfitUSDTSell = 0.0
    totalFee = 0
    for order in orders:
        orderData = json.loads(json.loads(json.dumps(order['order_data'])).replace("'", '"').replace('("binance",', '').replace('}}]})', '}}]}').replace('None', 'False').replace('False', 'false'))
        if (order['order_type'] == "SELL"):
            totalProfit += float(order['profit'])
            #Get 'cummulativeQuoteQty' for BUY order for executed SELL order
            buyOrder = getOrderByID(order['fixed_order_id'], ticker)
            buyOrderData = json.loads(json.loads(json.dumps(buyOrder['order_data'])).replace("'", '"').replace('("binance",', '').replace('}}]})', '}}]}').replace('None', 'False').replace('False', 'false'))
            #Add up all cummulativeQuoteQty for SELL orders and executed buy orders
            totalProfitUSDTBuy += float(buyOrderData['info']['cummulativeQuoteQty'])
            totalProfitUSDTSell += float(orderData['info']['cummulativeQuoteQty'])
            # todo [BUY] 24.9586797$ JE DAL ZA 0.000710 potem pa je [SELL] 0.000708 prodal za 25.05671472$ profit = 2 and 25.05671472-24.9586797=0.09803502
        totalFee += orderData['fee']['cost']
    return  totalProfit, totalFee , totalProfitUSDTBuy, totalProfitUSDTSell

print(binance.fetch_balance())
print("------------------------------")
#BTC
bnbPrice = getPrice("BNB/USDT")
#--

orders = getAllFinishedOrders("BTC/USDT")
totalProfit, totalFee , totalProfitUSDTBuy, totalProfitUSDTSell = getStats(orders, "BTC/USDT")

btcPrice = getPrice("BTC/USDT")
btcProfitInDolars = round(((btcPrice*totalProfit)-(bnbPrice*totalFee))+(totalProfitUSDTSell-totalProfitUSDTBuy),2)
btcProfitInDolarsNoFee = round(((btcPrice*totalProfit))+(totalProfitUSDTSell-totalProfitUSDTBuy),2)
print(f"BTC Bruto profit: {round(totalProfit,9)} BTC , {round(btcPrice*totalProfit,9)}$")
print(f"Fee:  {round(totalFee,6)} BNB,  {round(bnbPrice*totalFee,6)}$")
print(f"USDT SELL/BUY USDT difference (SELL/BUY) ({round(totalProfitUSDTSell,2)}$-{round(totalProfitUSDTBuy,2)}$):  {round(totalProfitUSDTSell-totalProfitUSDTBuy,2)}$")
print(f"NETO profit for BTC: {btcProfitInDolars}$" )
print(f"NETO profit for BTC (no fee): {btcProfitInDolarsNoFee}$" )
#49.01989358906845  *  0.064 = 2.75$ FEE
print("------------------------------")

orders = getAllFinishedOrders("TRX/USDT")
totalProfit, totalFee , totalProfitUSDTBuy, totalProfitUSDTSell = getStats(orders, "TRX/USDT")


trxPrice = getPrice("TRX/USDT")

trxProfitInDolars = round(((trxPrice*totalProfit)-(bnbPrice*totalFee))+(totalProfitUSDTSell-totalProfitUSDTBuy),2)
trxProfitInDolarsNoFee =  round(((trxPrice*totalProfit))+(totalProfitUSDTSell-totalProfitUSDTBuy),2)
print(f"TRX Bruto profit: {totalProfit} TRX, {round(trxPrice*totalProfit,9)}$")
print(f"Fee:  {round(totalFee,6)} BNB,  {round(bnbPrice*totalFee,6)}$")
print(f"USDT SELL/BUY USDT difference (SELL/BUY) ({round(totalProfitUSDTSell,2)}$-{round(totalProfitUSDTBuy,2)}$):  {round(totalProfitUSDTSell-totalProfitUSDTBuy,2)}$")
print(f"NETO profit for TRX: {trxProfitInDolars}$" )
print(f"NETO profit for TRX (no fee): {trxProfitInDolarsNoFee}$" )
print("-----------------TOTAL------------------")
print(f"***Total profit in $:  {round(btcProfitInDolars+trxProfitInDolars, 2)}$ ***")
print(f"***Total profit in $ without fee:  {round(btcProfitInDolarsNoFee+trxProfitInDolarsNoFee, 2)}$ ***")
#49.01989358906845