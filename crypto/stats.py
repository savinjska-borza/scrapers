import math
import ccxt
from time import sleep
import winsound
import pymysql
import json
import cachetools.func
#------------
from config import config

binance     = ccxt.binance({
    'apiKey': 'EG3rpPz9lcJEpDr5gAYG4dvyJTSF2JEglcnZ61vA9JT2cIyT1NlBXPnhAh65SIj9',
    'secret': 'FIoalbsfX7vu6Woi1UcBPQWGU57AUlYRWe6Pa4uAX63qnn08OtxFGraDxKHrwhIy',
    'timeout': 30000,
    'enableRateLimit': True,
})

configData = config().load()

def dbConnect():
    global c
    global conn
    try:
        c = None
    except:
        pass
    try:
        conn = None
    except:
        pass

    conn = pymysql.connect(
        db="crypto",
        user='root',
        passwd='',
        host='localhost',
        use_unicode=True, charset="utf8")
    c = conn.cursor(pymysql.cursors.DictCursor)
    c.execute('SET NAMES utf8;')
    c.execute('SET CHARACTER SET utf8;')
    c.execute('SET character_set_connection=utf8;')

dbConnect()




class Stats:
    def __init__(self):
        self.tickers = []
        self.statsList = []


        # Run
        self.getTickersFromConfig()
        self.getStats()
        self.printStats()
        print("-")

    @cachetools.func.ttl_cache(maxsize=128, ttl=10 * 60)
    def price(self, ticker):
        print(f"FETCH PRICE FOR {ticker}")
        return binance.fetch_ticker(ticker)['bid']

    def getTickersFromConfig(self):
        for item in configData['tradePairs']:
            if(item['active'] == True):
                self.tickers.append(item['ticker'])
        print(f"TICKERS TO BE PROCESSED: {self.tickers}")

    def cleanBinanceOrderData(self, binanceOrderData):
        return json.loads(json.loads(json.dumps(binanceOrderData)).replace("'", '"').replace('("binance",', '').replace('}}]})','}}]}').replace('None', 'False').replace('False', 'false'))

    def getAllFinishedOrders(self, ticker):
        """:arg
        get all finished BUY/SELL order pairs
        """
        c.execute(f"""SELECT * FROM buy_orders WHERE status LIKE 200 and ticker LIKE '{ticker}'""")
        data = c.fetchall()
        for item in data:
            c.execute(f"""SELECT * FROM sell_orders WHERE buy_order_id LIKE {item['id']} """)
            item['sellOrder'] = c.fetchone()
        return data


    def getStats(self):
        """:arg
        Got through all tickers.
        """
        for ticker in self.tickers:
            orders = self.getAllFinishedOrders(ticker)
            tickerStats = {'ticker': ticker,
                           'tickerQute': ticker.split("/")[0],
                           'totalProfit': 0.0,
                           'USDTBuyAmount': 0.0 ,
                           'USDTSellAmount': 0.0,
                           'totalProfitQuteCurrency': 0.0,
                           'USDTSellBuyDiff': 0.0,
                           'feeSell': 0.0,
                           'feeBuy': 0.0,
                           'fee': 0.0,
                           'bnbPrice': 0.0,
                           'feeUSD': 0.0,
                           'currentPriceForTicker': 0.0,
                           'profitUSD': 0.0,
                           'profitUSDNoFee': 0.0,
                           }
            for i, order in enumerate(orders):
                if(i==0):
                    i=1
                buyOrderData = self.cleanBinanceOrderData(order['order_data'])
                sellOrderData = self.cleanBinanceOrderData(order['sellOrder']['order_data'])
                # Add up all cummulativeQuoteQty for SELL orders and buy orders
                totalProfitQuteCurrency = float(order['sellOrder']['profit'])
                USDTBuyAmount = float(buyOrderData['info']['cummulativeQuoteQty'])
                USDTSellAmount = float(sellOrderData['info']['cummulativeQuoteQty'])
                USDTSellBuyDiff = round(USDTSellAmount - USDTBuyAmount,3)
                feeSell = float(sellOrderData['fee']['cost'])
                feeBuy =  float(buyOrderData['fee']['cost'])
                fee =  feeSell + feeBuy
                bnbPrice =  order['bnb_price']
                feeUSD =  round(bnbPrice * fee, 3)
                tickerStats['totalProfitQuteCurrency'] += totalProfitQuteCurrency
                tickerStats['USDTBuyAmount'] += USDTBuyAmount
                tickerStats['USDTSellAmount'] += USDTSellAmount
                tickerStats['USDTSellBuyDiff'] += USDTSellBuyDiff
                tickerStats['feeSell'] += feeSell
                tickerStats['feeBuy'] += feeBuy
                tickerStats['fee'] += fee
                tickerStats['numberOfTrades'] = i +1
                tickerStats['bnbPrice'] =  bnbPrice
                tickerStats['feeUSD'] +=  feeUSD
                tickerStats['currentPriceForTicker'] = self.price(ticker)
                tickerStats['profitUSD'] += round(((tickerStats['currentPriceForTicker'] *  totalProfitQuteCurrency) - (bnbPrice  *  fee)) + (USDTSellAmount - USDTBuyAmount), 3)
                tickerStats['profitUSDNoFee'] +=  round(((tickerStats['currentPriceForTicker'] * totalProfitQuteCurrency)) + (USDTSellAmount - USDTBuyAmount), 3)

            self.statsList.append(tickerStats)

    def printStats(self):
        totalNETOProfit = 0
        totalBRUTOProfit = 0
        totalTrades = 0
        for item in self.statsList:
            if ("numberOfTrades" not in item):
                continue
            totalNETOProfit += round(item['profitUSD'],2)
            totalBRUTOProfit += round(item['profitUSDNoFee'], 2)
            totalTrades += item['numberOfTrades']
            print(f"*----------{item['tickerQute']}----------*")
            print(f"Qute currency profit: {round(item['totalProfitQuteCurrency'],9)} {item['tickerQute']}")
            print(f"Fee:  {round(item['fee'],6)} BNB,  {round(item['feeUSD'],6)}$")
            print(f"USDT SELL/BUY USDT difference (SELL/BUY) {round(item['USDTSellBuyDiff'],2)}$")
            print(f"NETO profit for {item['tickerQute']}: {round(item['profitUSD'],2)}$")
            print(f"BRUTO profit for {item['tickerQute']}: {round(item['profitUSDNoFee'],2)}$")
            print(f"TRADES: {item['numberOfTrades']}")
            print(f"Average NETO profit peer trade: {round(item['profitUSD']/item['numberOfTrades'],2)}$")
        print("------------TOTAL PROFIT------------")
        print(f"NETO: {round(totalNETOProfit,2)}$")
        print(f"BRUTO: {round(totalBRUTOProfit,2)}$")
        print(f"TRADES: {totalTrades}")
        print(f"Average profit peer trade: {round(totalNETOProfit/totalTrades,3)}$")


Stats()

