import cachetools.func
import pymysql
from functools import wraps

conn = None
c = None


def connectToMySQL():
    global conn
    global c
    try:
        conn.close()
    except:
        pass
    try:
        c.close()
    except:
        pass
    conn = pymysql.connect(
        db="crypto",
        user='root',
        passwd='',
        host='localhost',
        use_unicode=True)
    c = conn.cursor(pymysql.cursors.DictCursor)
    c.execute('SET NAMES utf8;')
    c.execute('SET CHARACTER SET utf8;')
    c.execute('SET character_set_connection=utf8;')


def handleExceptions(fn):
    @wraps(fn)
    def wrapper(self, *args, **kw):
        try:
            connectToMySQL()
            return fn(self, *args, **kw)
        except Exception as e:
            #todo log to DB
            print(f"!!!!!!!![handleExceptions: {e}]!!!!!!!!!!!!")

    return wrapper

connectToMySQL()
class DB:

    @handleExceptions
    def getAllOpenBuyOrdersForTicker(self, ticker):
        try:
            c.commit()
        except:
            pass
        c.execute(f"""SELECT * FROM buy_orders WHERE status LIKE 0 AND ticker = '{ticker}'""")
        data = c.fetchall()
        return data

    @handleExceptions
    def updateBuyOrderStatus(self, buyOrderID):
        c.execute("UPDATE buy_orders SET status=200 WHERE id LIKE '{0}'".format(buyOrderID))
        conn.commit()

    @handleExceptions
    def addNewBuyOrder(self, ticker, order_data, market_price, quantity_qute_currency, strike_price, orderType, status, expected_profit, diff_stake , ticker_data, bnb_price):
        query = """
                  INSERT INTO  buy_orders (ticker, order_data, market_price, quantity_qute_currency, strike_price, order_type, status, expected_profit, diff_stake,ticker_data, bnb_price)
                  VALUES
                  (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
         """
        c.execute(query, (ticker, str(order_data), market_price, quantity_qute_currency, strike_price, orderType, status, expected_profit, diff_stake, ticker_data, bnb_price))
        conn.commit()
        return 200


    @handleExceptions
    def addNewSellOrder(self, ticker, order_data, market_price, quantity_qute_currency, strike_price, orderType, buy_order_id, profit, diff_stake , ticker_data, bnb_price):
        query = """
                  INSERT INTO  sell_orders (ticker, order_data, market_price, quantity_qute_currency, strike_price, order_type, buy_order_id, profit, diff_stake,ticker_data, bnb_price)
                  VALUES
                  (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
         """
        c.execute(query, (ticker, str(order_data), market_price, quantity_qute_currency, strike_price, orderType, buy_order_id, profit, diff_stake, str(ticker_data), bnb_price))
        conn.commit()
        return 200