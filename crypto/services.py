import winsound
from coinmarketcapapi import CoinMarketCapAPI, CoinMarketCapAPIError
import cachetools.func
import time

cmc = CoinMarketCapAPI('43906e39-4acd-463a-98e8-3e302c0d6033')


# ------------- SOUND --------------------------
class sound:
    def buy(self):
        frequency = 2300  # Set Frequency To 2500 Hertz
        duration = 500  # Set Duration To 1000 ms == 1 second
        winsound.Beep(frequency, duration)

    def sell(self):
        frequency = 1300  # Set Frequency To 2500 Hertz
        duration = 100  # Set Duration To 1000 ms == 1 second
        winsound.Beep(frequency, duration)
        frequency = 2300  # Set Frequency To 2500 Hertz
        duration = 100  # Set Duration To 1000 ms == 1 second
        winsound.Beep(frequency, duration)
        frequency = 1300  # Set Frequency To 2500 Hertz
        duration = 100  # Set Duration To 1000 ms == 1 second
        winsound.Beep(frequency, duration)


# ------------- CCXT --------------------------
class exchange:
    def getQuantityOfQuteCurrency(self, exchange, marketPrice, symbol, percentageIncrease=2):
        """
        Calculate minimal quantity + percentage increase of QuoteCurrency
        :param marketPrice: Market price of QuoteCurrency
        :param symbol: Ticker e.g TRX/USDT
        :param percentageIncrease: Percentage increase of minimal required value
        e.g  exchange, 0.03145, TRX/USDT , 50
        :return: Quantity of qute currency e.g 401
        """
        marketTicker = exchange.markets[symbol]
        minCost = round(marketTicker['limits']['cost']['min'] / marketPrice, 6)
        minCost = format(float(minCost) + float(percentageIncrease * minCost) / 100, 'f')
        return minCost

    def getTickers(self, exchange, tickers):
        print("GET TICKERS")
        """:arg
        :param exchange: exchange instance
        :param tickers: ['BTC/USDT', 'TRX/USDT']
        """
        return exchange.fetchTickers(tickers)


    def getOhlcv(self, exchange, ticker, timeFrame = "1m"):
        print("GET getOhlcv")
        """:arg
        :param exchange: exchange instance
        :param ticker: 'BTC/USDT'
        :parm timeframe 1m, 5m, 1h, 1d, 1M
        """

        b = exchange.fetch_ohlcv(ticker, timeFrame)
        datetime = time.strftime("%Y-%m-%dT%H:%M:%S", time.localtime(int(str(b[-1][0])[:-3])))
        openPrice = b[-1][1]
        highestPrice = b[-1][2]
        lowestPrice = b[-1][3]
        losingPrice = b[-1][4]
        volume = b[-1][4]
        return datetime, openPrice, highestPrice, lowestPrice, losingPrice, volume




# ---------------------------------------------
# ------------- COINMARKET CAP-----------------
class coinMarketCap:
    @cachetools.func.ttl_cache(maxsize=128, ttl=10 * 1200)
    def getCurrentPriceForSymbolFromCoinMarketCap(self,binance):
        try:
            r = cmc.cryptocurrency_listings_latest()
            a = r.data
            for item in a:
                if (item['symbol'] == "BNB"):
                    price = item['quote']['USD']['price']
                    return price
        except Exception as e:
            print(f"COINMARKET CAP: {e}")
            return binance.fetch_ticker('BNB/USDT')['bid']
# ----------------------------------------------
