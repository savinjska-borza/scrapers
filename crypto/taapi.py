import  requests

# Define indicator
indicator = "ema"

# Define endpoint
endpoint = f"https://api.taapi.io/{indicator}"

# Define a parameters dict for the parameters to be sent to the API
parameters = {
    'secret': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InJhbWJvc3MxMzM3QGdtYWlsLmNvbSIsImlhdCI6MTYxMjI4OTI1OSwiZXhwIjo3OTE5NDg5MjU5fQ.mPOEa-it2zO8YvMMEq9dzGVGOWj_wBr-1N2Zl8TC4wg',
    'exchange': 'binance',
    'symbol': 'BTC/USDT',
    'interval': '5m'
}

# Send get request and save the response as response object
response = requests.get(url=endpoint, params=parameters)

# Extract data in json format
result = response.json()

# Print result
print(result)