


def getMarketPrice(exchange, symbol):
    orderbook = exchange.fetch_order_book (exchange.markets[symbol]['symbol'] )
    bid = orderbook['bids'][0][0] if len (orderbook['bids']) > 0 else None
    return bid

