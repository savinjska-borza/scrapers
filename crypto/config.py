import json


class config:
    """:arg
    Load and return JSON config as dictionary.
    """

    def load(self):
        with open('config.json') as f:
            data = json.load(f)
        return data
