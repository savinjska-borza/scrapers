import math
import ccxt
from time import sleep
import pymysql
import json
from coinmarketcapapi import CoinMarketCapAPI, CoinMarketCapAPIError
import cachetools.func
import asyncio
##-------LOCAL----------
from services import sound, exchange, coinMarketCap
from config import config
from database import DB
# -----------------------

cmc = CoinMarketCapAPI('43906e39-4acd-463a-98e8-3e302c0d6033')
binance = ccxt.binance({
    'apiKey': 'EG3rpPz9lcJEpDr5gAYG4dvyJTSF2JEglcnZ61vA9JT2cIyT1NlBXPnhAh65SIj9',
    'secret': 'FIoalbsfX7vu6Woi1UcBPQWGU57AUlYRWe6Pa4uAX63qnn08OtxFGraDxKHrwhIy',
    'timeout': 30000,
    'enableRateLimit': True,
})

# Read config
configData = config().load()
allTickers = set()
#
dryRun = False




# ------------------------------FUNCTIONS-------------------
async def reloadConfig():
    global configData
    configData = config().load()

def tickTasks(ticksFromStart):
    #Every 60 ticks reload
    if(ticksFromStart % 60 == 0):
         reloadConfig()
    return

def openBuyOrders(ticker):
        return DB().getAllOpenBuyOrdersForTicker(ticker)

def processTickerData(ticker):
    marketPrice = ticker['last'] #Price of last trade
    high = ticker['high'] #Highest price this period
    low =  ticker['low'] #Lowest price this period
    vwap = ticker['vwap'] #Lowest price this period
    return marketPrice, high, low, vwap

def setBuyPrice(marketPrice, buyPriceFloor, tradePair):
    if(buyPriceFloor is None or buyPriceFloor <= 0):
        buyPriceFloor = marketPrice - tradePair['decreseBuyPriceAtStart'] #After bot starts
    if(abs(float(marketPrice)-float(buyPriceFloor))>tradePair['diffInBaseCurrencyToResetBuyPrice']):
        print(f"BUY PRICE RESETED FOR {tradePair['ticker']}... OLD/NEW BUY PRICE: {buyPriceFloor}/{marketPrice - tradePair['decreseBuyPriceAtStart']}")
        buyPriceFloor = marketPrice - tradePair['decreseBuyPriceAtStart']
    return buyPriceFloor

def buy(quantityOfQuteCurrencyToBuy, marketPrice, buyPriceFloor, tickerData, tradePair):
    # Izvedem nakup če je bid price (buy) majnša ali enaka definirani ter če ni zadnji nakup bil BUY
    bnbPrice = coinMarketCap().getCurrentPriceForSymbolFromCoinMarketCap(binance)
    binanceOrder = binance.id, binance.create_market_buy_order(tradePair['ticker'], quantityOfQuteCurrencyToBuy)
    print(f"****BUY ORDER EXECUTED: MARKET PRICE: {marketPrice}, NUMBER OF {tradePair['ticker'].split('/')[0]}: {quantityOfQuteCurrencyToBuy}")
    strikePrice = float(marketPrice) + float(tradePair['diffStakeInBaseCurrencyToSell'])
    DB().addNewBuyOrder(tradePair['ticker'], str(binanceOrder), str(marketPrice), str(quantityOfQuteCurrencyToBuy), strikePrice, "MARKET", 0, 0, tradePair['diffStakeInBaseCurrencyToSell'], str(tickerData), bnbPrice)
    buyPriceFloor = buyPriceFloor - tradePair['decreesBuyPriceAfterBuy']
    buyTicks = tradePair['ticksToWaitBeforeNextBuy']
    if(tradePair['sound'] == True):
        sound().buy()
    return  buyPriceFloor, buyTicks

def sell(marketPrice, quantityOfQuteCurrencyToBuy, buyMarketOrders, tradePair, buyPriceFloor, tickerData):
    """:arg
    #It will 'close' orders in DB (close = sell at profitable price)
    """
    for order in buyMarketOrders:
        orderSellPrice = float(order['market_price']) + tradePair['diffStakeInBaseCurrencyToSell'] #todo variabble percentage depends on volume itd (to make more profit)
        if(marketPrice>=orderSellPrice):
            bnbPrice = coinMarketCap().getCurrentPriceForSymbolFromCoinMarketCap(binance)
            #Izvedem sell če je bid price (buy) večja ali enaka definirani ter če ni zadnji nakup bil SELL
            binanceOrder = binance.id, binance.create_market_sell_order(tradePair['ticker'], quantityOfQuteCurrencyToBuy)
            print(f"****SELL ORDER EXECUTED: MARKET PRICE: {marketPrice}, NUMBER OF {quantityOfQuteCurrencyToBuy}: {tradePair['ticker'].split('/')[0]}, CLOSE BUY ORDER ID: {order['id']}")
            profit = format(float(order['quantity_qute_currency']) - float(quantityOfQuteCurrencyToBuy), "f")
            buyPriceFloor = buyPriceFloor - float(tradePair['decreesBuyPriceAfterBuy']) #Set buy price #todo buy price range... definiran rang nad kolko ne kupuje
            tradePair['diffStakeInBaseCurrencyToSell']  = float(tradePair['diffStakeInBaseCurrencyToSell']) + float(tradePair['ValueInBaseCurrencyToIncreaseDiffStakeAfterSell'])
            status = DB().addNewSellOrder(tradePair['ticker'], str(binanceOrder), str(marketPrice), str(quantityOfQuteCurrencyToBuy), "SELL", 1, order['id'], profit,  tradePair['diffStakeInBaseCurrencyToSell'] , tickerData, bnbPrice)
            if(status == 200):
                DB().updateBuyOrderStatus(order['id'])
                if (tradePair['sound'] == True):
                    sound().sell()
    return buyPriceFloor



@cachetools.func.ttl_cache(maxsize=512, ttl=1.5)
def getTickers():
    print("getTickers")
    return exchange().getTickers(binance, tuple(allTickers))

@cachetools.func.ttl_cache(maxsize=512, ttl=60)
def getOhlcv(ticker):
    print("getOhlcv")
    return exchange().getOhlcv(binance, ticker)
    
#--------------BUY STRATEGIES--------------
def buyPriceUnderMarketPrice(marketPrice, buyPriceFloor, numberOfOpenBuyOrders, tradePair, buyTicks, quantityOfQuteCurrencyToBuy, tickerData):
    if (marketPrice <= buyPriceFloor and numberOfOpenBuyOrders < tradePair['maxNumberOfBuyOrders'] and buyTicks < 1 and buyPriceFloor <= tradePair['buyPriceCeiling']):
        buyPriceFloor, buyTicks = buy(quantityOfQuteCurrencyToBuy, marketPrice, buyPriceFloor, tickerData, tradePair)
    return buyPriceFloor, buyTicks



# *********************************************************#
# --------------------Exchange manager---------------------
async def manager(tradePair):  # LOOP
    print(f"===> MANAGER STARTED FOR  {tradePair['ticker']} <===")
    ticksFromStart = 0
    buyTicks = tradePair['ticksToWaitBeforeNextBuy']
    buyPriceFloor = - tradePair['decreseBuyPriceAtStart'] #Decrese buy price when bot start
    while (1):
        try:
            await asyncio.sleep(configData['tickTimeLength'])
            # ----------------DATA-----------------------------------
            openBuyOrdersForTicker =  openBuyOrders(tradePair['ticker'])
            numberOfOpenBuyOrders =  len(openBuyOrdersForTicker) #Count all open orders (BUY) for this ticker
            tickerData = getTickers()[tradePair['ticker']]
            OhlcvDatetime, OhlcvOpenPrice, OhlcvHighestPrice, OhlcvLowestPrice, OhlcvLosingPrice, OhlcVvolume = getOhlcv(tradePair['ticker'])
            marketPrice, high, low, vwap =  processTickerData(tickerData) #Set data for current tick
            quantityOfQuteCurrencyToBuy = exchange().getQuantityOfQuteCurrency(binance, marketPrice, tradePair['ticker'], tradePair['percentageIncreaseofQuoteCurrencyForBuying'])
            buyPriceFloor = setBuyPrice(marketPrice, buyPriceFloor, tradePair) #Price under which BUY order will be executed
            print(f"TICK TACK  {tradePair['ticker']} Buy ticks: {buyTicks}, MarketPrice: {marketPrice} Buy price {buyPriceFloor}, Buy diff price: {marketPrice-buyPriceFloor}  Open orders {numberOfOpenBuyOrders}/{tradePair['maxNumberOfBuyOrders']} <========")
            #--------------BUY---------------------------------------
            buyPriceFloor, buyTicks = buyPriceUnderMarketPrice(marketPrice, buyPriceFloor, numberOfOpenBuyOrders, tradePair, buyTicks, quantityOfQuteCurrencyToBuy, tickerData) #Buy and set new buyPriceFloor
            # --------------SELL---------------------------------------
            buyPriceFloor = sell(marketPrice, quantityOfQuteCurrencyToBuy, openBuyOrdersForTicker, tradePair, buyPriceFloor, tickerData)
            # ---------------------------------------------------
            ticksFromStart += 1
            buyTicks -= 1
            # ------Perform tasks every specific tick
            tickTasks(ticksFromStart)
        except Exception as e:
            print(f"ERROR: {e}")
            await asyncio.sleep(10)


async def main():
    print(f"***************************************************")
    print(f"               PANCIR {configData['version']}")
    print(f"***************************************************")
    tasks = []
    for i in range(len(configData['tradePairs'])):
        if(configData['tradePairs'][i]['active'] == True):
            allTickers.add(configData['tradePairs'][i]['ticker'])
            tasks.append(asyncio.ensure_future(manager(configData['tradePairs'][i])))
    # Start executing all tasks (pairs)
    await asyncio.gather(*tasks)


# Start
loop = asyncio.get_event_loop()
try:
    loop.run_until_complete(main())
finally:
    loop.close()
