import cachetools.func
import functools
from flaskext.mysql import MySQL
# MySQL configurations
c = None
conn = None

def mySqlConnection(app):
    global c
    global conn
    # if conn and conn.open:
    #      return True
    mysql = MySQL()
    app.config['MYSQL_DATABASE_USER'] = 'root'
    app.config['MYSQL_DATABASE_PASSWORD'] = ''
    app.config['MYSQL_DATABASE_DB'] = 'stockbot'
    app.config['MYSQL_DATABASE_HOST'] = 'localhost'
    try:
        mysql.init_app(app)
    except Exception as e:
        pass
    conn = mysql.connect()
    c = conn.cursor()
    return True

# class add:
#
#     def ark(self, app, conn, name):
#         query = """
#                       INSERT INTO  ark (product_name)
#                       VALUES
#                       (%s)
#              """
#         c.execute(query, (name))
#         conn.commit()
#         return 200


class get:

    def convertResultToDict(self, data):
        if(data is None):
            return None
        fields = list(map(lambda x: x[0], c.description))
        dataRes = [dict(zip(fields, row)) for row in data]
        return dataRes

    @cachetools.func.ttl_cache(maxsize=128, ttl=10 * 60)
    def data(self, colum=None, value=None, tableName = None):
        """:arg

        """
        # Get all
        if (colum is None and value is None):
            c.execute("""SELECT * FROM {0} """.format(tableName))
            data = c.fetchall()
            return self.convertResultToDict(data)
        # Get with filter
        c.execute("""SELECT * FROM {0} WHERE {1} LIKE '{2}' """.format(tableName, colum, value))
        data = c.fetchall()
        return self.convertResultToDict(data)

    @cachetools.func.ttl_cache(maxsize=128, ttl=10 * 60)
    def columns(self,  tableName):
        """:arg

        """
        c.execute(""" SHOW COLUMNS FROM `{0}` """.format(tableName))
        data = c.fetchall()
        data = [col[0] for col in data]
        return data

